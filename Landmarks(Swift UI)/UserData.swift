//
//  UserData.swift
//  Landmarks(Swift UI)
//
//  Created by Mark Senich on 6/4/19.
//  Copyright © 2019 Austin senich. All rights reserved.
//

import SwiftUI
import Combine

final class UserData: BindableObject {
    let didChange = PassthroughSubject<UserData, Never>()
    
    var showFavoritesOnly = false {
        didSet {
        didChange.send(self)
        }
    }
    var landmarks = landmarkData {
        didSet {
            didChange.send(self)
        }
    }
    
    
    
}
