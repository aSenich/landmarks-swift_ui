//
//  Row.swift
//  Landmarks(Swift UI)
//
//  Created by Mark Senich on 6/4/19.
//  Copyright © 2019 Austin senich. All rights reserved.
//

import SwiftUI

struct LandmarkRow : View {
    var landmark: Landmark
    
    var body: some View {
        HStack {
            CircleImage(image: landmark.image(forSize: 50))
            
        Text(landmark.name)
            
            Spacer()
            if landmark.isFavorite {
                Image(systemName: "star.fill")
                .imageScale(.medium)
                .foregroundColor(.yellow)
            }
            
        }
    }
}

#if DEBUG
struct Row_Previews : PreviewProvider {
    static var previews: some View {
        Group {
            LandmarkRow(landmark: landmarkData[0])
            LandmarkRow(landmark: landmarkData[1])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
#endif
